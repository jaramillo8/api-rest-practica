<?php

namespace Tests\Feature;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContacApiTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    // se crea un constructor con setUp para poder crear un usuario para las pruebas necesarias
    public function setUp():void {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user, 'api');
    }   

    /**
     * Prueba que permite verificar si se puede crear un contacto, con el assertStatus 201 y si se responde un json con la una llave data
     */
    public function test_can_create_contact() {

        $formData = [
            'name' => 'Ivan',
            'email' => 'ivanciitojf@gmail.com'
        ];

        $this->withoutExceptionHandling();

        $this->json('POST', route('contacts.store'),$formData)->assertStatus(201)->assertJson(['data' => $formData]);
    }

    /**
     * Prueba que nos permite verificar si se puede obtener un contacto en especifico, con el assertStatus 200
     */
    public function test_can_show_contac(){
        $contact = Contact::factory()->make();
        $this->user->contacts()->save($contact);
        $this->get(route('contacts.show', $contact->id))->assertStatus(200);
    }

    /**
     * Prueba que permite verificar si es posible actualizar un contacto en especifico, esperando un assertStatus 200 y un assertJson con una llave data
     */
    public function test_can_update_contact() {
        $contact = Contact::factory()->make();
        $this->user->contacts()->save($contact);

        $updatedData = [
            'name' => 'Enrique',
            'email' => 'ivanciitojf@gmail.com'
        ];

        $this->json('PUT', route('contacts.update', $contact->id), $updatedData)
             ->assertStatus(200)
             ->assertJson(['data' => $updatedData]);
    }

    /**
     * Prueba que permite verificar si es posible listar todos los contactos del usuario, esperando un assertStatus 200
     */
    public function test_can_list_contacts(){
        $contacts = Contact::factory()->count(3)->make();
        $this->user->contacts()->saveMany($contacts);
        $this->get(route('contacts.index'))->assertStatus(200);
    }

    /**
     * Prueba que permite verificar si se puede eliminar un contacto en especifico. Esperando un assertStatus 200
     */
    public function test_can_delete_contact(){
        $contact = Contact::factory()->make();
        $this->user->contacts()->save($contact);
        $this->delete(route('contacts.destroy', $contact->id))->assertStatus(200);
    }
}
