<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\StatController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/**
 * Grupo de rutas que sirven para realizar el registro y la autenticación del usuario para poder registrar contactos.
 */
Route::group([
    'middleware' => 'api',
    'prefix' => 'v1/auth'
], function(){
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('logout', [AuthController::class, 'logout']);
});

/**
 * Grupo de rutas que permitirán al usuario logeado, poder registrar contactos en su aplicación
 */
Route::group([
    'middleware' => 'api',
    'prefix' => 'v1'
], function(){
    Route::apiResource('contacts', ContactController::class);
});

/**
 * Ruta que permite acceder a las estadísticas de la api.
 */

 Route::group([
     'prefix' => 'v1'
 ], function(){
     Route::get('stats',[StatController::class,'index']);
 });