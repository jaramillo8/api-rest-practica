## Prueba Api Rest Aiwifi

---

### Prerequisitos:

1. Software necesario.

    a) PHP versión "^7.3|^8.0"

    b) Composer.

    c) Crear un cuenta para obtener un APIKey en [mailboxlayer](https://mailboxlayer.com/) necesaria para la validación del correo electrónico.

2. Una vez se clone el repositorio se debe renombrar o hacer una copia de archivo **.env.example** y llamarlo solo **.env**
3. Ejecutar el comando `composer install` para instalar los requerimientos necesarios.
4. Ejecutar el comando `php artisan key:generate` para generar un **APP_KEY**
5. Crear un BD local y remplazar las llaves **DB_DATABASE** , **DB_USERNAME** , **DB_PASSWORD** en el archivo **.env**
6. Agregar al archivo **.env** una llave con nombre **MAILBOX_LAYER_API_KEY** que se obtiene al crear un cuenta gratis en la pagina antes mencionada.
7. Ejecutar el comando `php artisan jwt:secret` para obtener una llave **JWT_SECRET** que se crea en el archivo **.env**
8. Ejecutar el comando `php artisan migrate` para migrar y crear las tablas necesarias.
9. Para levantar el servidor local se debe ejecutar el comando `php artisan serve`.

**Los dependencias utilizadas fueron las siguientes:**

-   [jwt-auth](https://jwt-auth.readthedocs.io/en/develop/)
-   [Laravel Mailbox Layer](https://github.com/ash-jc-allen/laravel-mailboxlayer)

---

# Endpoints

### 1. Registro Usuario

**Descripción:** Sirve para el registro de un usuario nuevo que puede utilizar el api.

`POST /api/v1/auth/register`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con la información del usuario creado.
-   Se require enviar en formato JSON tres parámetros **name** , **email** y **password**

### 2. Login Usuario

**Descripción:** Sirve para poder hacer login de un usuario previamente registrado.

`POST /api/v1/auth/login`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con el `token` y el tipo de token.
-   El `token` que se obtiene servirá para realizar las acciones en la api.

### 3. Creación de Contacto

**Descripción:** Sirve para crear un nuevo contacto del usuario registrado y logueado.

`POST /api/v1/contacts`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con la información del contacto y el identificador del usuario que ha creado el contacto.
-   Se require enviar en formato JSON tres parámatros **name**, **email** y **token** este último obtenido una vez al hacer login.
-   El `token` pude enviarse como parámetro o bien como parte de la opción `Authorization` y en el **type** `Bearer Token`

### 4. Obtener los contactos del usuario.

**Descripción:** Sirve para obtener los contactos registrados del usuario logueado.

`GET /api/v1/contacts`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con la información de todos los contactos registrados.
-   Se require enviar como parámetro el `token` que obtuvo el usuario al realizar el login.
-   El `token` pude enviarse como parámetro o bien como parte de la opción `Authorization` y en el **type** `Bearer Token`

### 5. Obtener un contacto en especifico.

**Descripción:** Sirve para obtener la información de algún contacto en especifico por su identificador.

`GET /api/v1/contacts/1`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con la información del contacto solicitado.
-   Se require enviar el `token`.
-   El `token` pude enviarse como parámetro o bien como parte de la opción `Authorization` y en el **type** `Bearer Token`

### 6. Actualizar Contacto

**Descripción:** Sirve para actualizar la información de un contacto en especifico.

`PUT /api/v1/contacts/1`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con la información del contacto actualizada.
-   Se require enviar el valor **name** y/o **email** así como el `token`, para poder actualizar la información.
-   El `token` pude enviarse como parámetro o bien como parte de la opción `Authorization` y en el **type** `Bearer Token`

### 7. Eliminar Contacto

**Descripción:** Sirve para eliminar la información de un contacto en especifico.

`DELETE /api/v1/contacts/1`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con un mensaje para verificar si se ha eliminado el contacto.
-   Se require enviar el `token`
-   El `token` pude enviarse como parámetro o bien como parte de la opción `Authorization` y en el **type** `Bearer Token`

### 8. Estadísticas API

**Descripción:** Sirve para obtener estadísticas del API, como el total de usuarios registrados, total de contactos, etc...

`GET /api/v1/stats`

Consideraciones:

-   El endpoint regresa una respuesta **JSON** con información de estadísticas del API.
-   El endpoint es publíco, no require hacer login o enviar el `token`

---

**Nota:**

    * Ejemplo de como es que se debe enviar información a los endpoints.

    * {
        "name":"Ivan",
        "email":"ivan@mail.com",
        "token":"yJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiO"
      }

---

## Pruebas

-   Para realizar las pruebas como lo marca el archivo `.env.testing` se debe crear una base de datos `sqlite`
-   El directorio donde se debe crear esta base de datos es dentro de la carpeta `database` y ahí crear un archivo `test_api.sqlite`
-   una vez hecho esto se pueden correr las pruebas con el comando `./vendor/bin/phpunit`
