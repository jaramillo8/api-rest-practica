<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'email',
    ];

    //Método que nos permite realizar la relación entre los contactos y el usuario al cual le pertenecen.
    public function user(){
        return $this->belongsTo(User::class);
    }
}
