<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\User;
use AshAllenDesign\MailboxLayer\Facades\MailboxLayer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * El StatController serve para poder acceder a la función index que nos regresa la información correspondiente a la estadísticas solicitadas.
 */
class StatController extends Controller
{
    //

    public function index(){
        // Se obtiene el total del usuarios registrados
        $totalUsers = DB::table('users')->get()->count();
        //Se obtiene el total de contactos almacenados. 
        $totalContacts = DB::table('contacts')->get()->count();
        // Se obtienen todos los emails de los contactos para hacer la validación
        $emailsContact = DB::table('contacts')->select('email')->get();
        // Se obtiene el promedio de contactos
        $averange = DB::table('contacts')->avg('user_id');
        // la variable $emailsFreeCount nos ayudara a guardar el total de emails que coincidan con la validación que a continuación se describe.
        $emailsFreeCount = 0;

        /**
         * Se realiza la iteración de para validar si el email del contacto es gratuito.
         */
        for($i = 0; $i < $totalContacts; $i++){
            //Se realiza la validación del mail con la ayuda de la dependencia MailboxLayer
            $validated = MailboxLayer::check($emailsContact[$i]->email);
            //Si la validación en la llave free es verdadera
            if($validated->free){
                // se incrementa la variable $emailsFreeCount en uno, para al final saber la cantidad total de emails gratuitos.
                $emailsFreeCount = $emailsFreeCount + 1;
            }
        }

        /**
         * Se retorna una respuesta json con los valores de total de usuarios registrados, total de contactos, promedio de contactos y total de emails gratuitos.
         */
        return response()->json(['data' => ['totalUsers' => $totalUsers, 'totalContact' => $totalContacts, 'averange' => $averange, 'totalFreeEmails' => $emailsFreeCount]],200);
    }
}
