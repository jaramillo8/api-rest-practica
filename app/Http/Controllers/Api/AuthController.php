<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * El AuthController se encargará de realizar las acciones pertinentes en cuanto a la autenticación y registro del usuario
 */

class AuthController extends Controller
{
    /**
     * Se le asigna al método constructor el middleware 'auth:api' excepto a los métodos login y register que el usuario debe tener acceso para poder registrarse y hace login 
     */
    public function __construct() {
        $this->middleware('auth:api',['except' => ['login','register']]);
    }

    /**
     * El método register nos sirve para realizar el registro del usuario en la BD
     */
    public function register(Request $request) {
        //Se realiza la validación de los datos que vienen del $request con las reglas descritas para cada atributo.
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
        ]);

        // Si se encuentra algún fallo en la validación 
        if($validator->fails()){
            // Se retorna un respuesta json con un mensaje de error y los errores descritos, por ejemplo: que los atributos no deben estar vacíos o que deben cumplir con el tipo de dato requerido.
            return response()->json(['message' => 'Upps, something was wrong','errors' => $validator->errors()],422);
        }

        // Ya que tenemos la validación hecha y han pasado las reglas de validación.
        //Se crea un nuevo usario con el el arreglo $validator y los datos validados, así como también en la llave password se procede a encriptar el password con ayuda del bcrypt()
        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        // Hecha la creación del usario se retorna una respuesta json con un mensaje de éxito y con la información del usuario creado, y un code status de 201

        return response()->json(['message' => 'User created successfully!','data' =>$user],201);
    }

    /**
     * El método login nos servirá para poder loggear a cada usuario y poder entregar un token para que pueden tener acceso a crear contactos en su aplicación
     */
    public function login(Request $request){

        //Realizamos la validación de los datos recibidos por el $request con las reglas de validación descritas.
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required|string|min:6'
        ]);

        // Si la validación tiene alguna falla
        if($validator->fails()){
            // Se retorna una respuesta json con un mensaje de error y los errors que se encontraron, por ejemplo: que los datos esten vacíos 
            return response()->json(['message' => 'Upps, something was wrong','errors' => $validator->errors()],400);
        }

        //Se crea una variable que nos permitirá guardar el token creado.
        $jwt_token = null;

        //Se verifica si se puede crear el token con los datos validados, es decir si los datos ingresados coinciden con los datos registrados en la BD
        if(!$jwt_token = JWTAuth::attempt($validator->validated())){
            //Si no coinciden los datos, se retorna una respuesta json con un mensaje de error, diciendo que alguno de los datos ingresados son incorrectos
            return response()->json(['message' => 'Upps, something was wrong, The email or password field are incorret','error' => 'Unauthorized',401]);
        }

        // Si los datos coinciden se retorna una respuesta json con el token obtenido para el usuario y con el tipo del token.
        return response()->json([
            'token' => $jwt_token,
            'token_type' => 'bearer',
        ],200);
    }

    /**
     * El método logout permite al usuario hacer el logout de su applicación
     */
    public function logout() {
        Auth::logout();
        return response()->json(['message' => 'User logged out successfully']);
    }
}
