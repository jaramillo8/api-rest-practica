<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactResource;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use AshAllenDesign\MailboxLayer\Facades\MailboxLayer;

class ContactController extends Controller
{
    // Se asigna el middleware auth:api
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * El método index despliega una lista de los contactos almacenados en BD y que pertenecer al usuario logeado
     *
     * @return ContactResource
     */
    public function index()
    {
        
        // Se retorna un ContactResource con la información encontrada
        return ContactResource::collection(auth()->user()->contacts()->get());
    }

    

    /**
     * El método store nos permite almacenar un nuevo registro en la BD 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @return ContactResource
     */
    public function store(Request $request)
    {
        // Se validan los datos obtenidos del $request que son el name y el email
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|email'
        ]);

        //Si hay un fallo en la validación con respecto a los datos se retorna una respuesta con un mensaje de error y con los errores que se encontraron el la validación

        if($validator->fails()){
            return response()->json(['message' => 'Upps, something was wrong','error' => $validator->errors()],400);
        }

        // Se crea la variable $mailValidator para realizar la validación del mail, con la ayuda del facade MailboxLayer y el método check que recibe como parámetro el email que se obtiene del $request
        $mailValidator = MailboxLayer::check($request->input('email'));

        //Verificamos lo que el método check nos retorna
        // si la respuesta en la propiedad smtpCheck is false querrá decir que no es un email valido, que el email no existe.
        if(!$mailValidator->smtpCheck){
            // Si eso pasa se retorna un respuesta, en la que se pide ingresar un email valido.
            return response()->json(['message' => 'Upps, something was wrong, you need to write a valid mail'],422);
        }

        //En caso de que las anteriores validaciones pasen satisfactoriamente, se procede a crear el nuevo contacto del usuario loggeado
        $contact = auth()->user()->contacts()->create($validator->validated());
        // se retorna la información del contacto creado
        return new ContactResource($contact);
    }

    /**
     * Se muestra un contacto en especifico
     *
     * @param  Contact  $contact
     * @return ContactResource
     */
    public function show(Contact $contact)
    {
        // se retorna el contacto que el usuario ha introducido.
        return new ContactResource($contact);
    }


    /**
     * Método para actualizar un registro en especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Contact  $contact
     * @return ContactResource
     */
    public function update(Request $request, Contact $contact)
    {
        // Se validan los datos obtenidos del $request que son el name y el email
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|email'
        ]);

        //Si hay un fallo en la validación con respecto a los datos se retorna una respuesta con un mensaje de error y con los errores que se encontraron el la validación

        if($validator->fails()){
            return response()->json(['message' => 'Upps, something was wrong','error' => $validator->errors()],400);
        }

        // Se crea la variable $mailValidator para realizar la validación del mail, con la ayuda del facade MailboxLayer y el método check que recibe como parámetro el email que se obtiene del $request
        $mailValidator = MailboxLayer::check($request->input('email'));

        //Verificamos lo que el método check nos retorna
        // si la respuesta en la propiedad smtpCheck is false querrá decir que no es un email valido, que el email no existe.
        if(!$mailValidator->smtpCheck){
            // Si eso pasa se retorna un respuesta, en la que se pide ingresar un email valido.
            return response()->json(['message' => 'Upps, something was wrong, you need to write a valid mail'],422);
        }

        $contact->update($validator->validated());

        return new ContactResource($contact);
    }

    /**
     * El método destroy elimina un contacto en especifico
     *
     * @param  Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        if($contact->delete()){
            return response(['message' => 'Contact Deleted']);
        }else{
            return response(['message' => 'Contact cannot deleted']);
        }
    }
}
